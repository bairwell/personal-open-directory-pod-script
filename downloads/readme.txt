
Contents        : Personal Open Directory for Dmoz.Org
Version         : v1.95 - 25 Mar 2004
Authors         : John M. Grohol and others
Downloaded from : http://grohol.com/downloads
Requires        : Perl 5.004
                  Also needs following modules: CGI, HTTP::Request,
                                     HTTP:Response, LWP::UserAgent

## 1.95 
## International fixes courtesy of John Campbell

## 1.92
## Fixes to ensure program is following the ODP's terms and conditions

## 1.90
## Minor bug fixes, including new timeout script

## 1.88
## Fixed search results showing up in green

## 1.87
## Fixed improper display of dmoz logo in Kids directory

## 1.86
## Fixed improper display of ODP attribution box (this one got by us, sorry)

## 1.81
## Bug fixes

## 1.80
## Bug fixes

## [new in v1.79d]
## Single bug fix (search for 1.79 in file for specific line changes)

## [new in v1.78d]
## Single bug fix (search for 1.78 in file for specific line changes)

## [new in v1.77d]
## Single bug fix (search for 1.77 in file for specific line changes)

## [new in v1.76d]
## Misc bug fixes (search for 1.76 in file for specific line changes)
                 
## [new in v1.75d]
## Misc bug fixes (search for 1.75 in file for specific line changes)
 
## [new in v1.72d]
## If you use SSI to include the output of POD, then pod_headersuppress
## needs to be set to '1' to suppress the redudent headers which corrupt
## the SSI output.
# $pod_headersuppress='1';   

*** NOTE: ***

Rename all .txt files as follows:

    pod.txt        -> pod.cgi
    podvars.pm.txt -> podvars.pm
    servertest.txt -> servertest.pl



