===========================================================================
== 2.0     What this program does
===========================================================================

 - Ever wanted to provide a web directory to your users, but you don't
   just want to link to the wonderful Open Directory Project - aka
   Dmoz.org ( http://dmoz.org/ ) ?
 - Can't afford the bandwidth to download, process and re-upload over
   100Mb of the RDF files?
 - Can't afford/don't want to spend two hundred dollars for a program
   that pulls the pages off the Dmoz.Org server and reformats them?
   
         Then POD (Personal Open Directory) is for you!
         
The Personal Open Directory calls the Open Directory Project server
to provide you and your users with direct access to the fastest growing
human edited directory on the entire internet. This isn't done by
using frames (which aren't supported by all browsers, nor liked by
many users), or parsing the huge RDFs from the ODP itself, but rather
grabbing the data in real-time from the ODP.


===========================================================================
== 2.0.1   Features
===========================================================================

The Personal Open Directory has the following features:
 - Direct, up to date, access to the Open Directory Project
 - Completely customizable output
 - Complete searching of the ODP available
 - Automated linking to subjects at Amazon.Com (when used in conjunction
   with the 'Maxcomm' script - available from http://grohol.com/downloads )
 - Proxy support (so you can not only reduce your servers bandwidth
   usage, but also that of Dmoz.Org)
 - Cache friendly (returns Expires: header along with Content-Length:)
 - Returns the Referer: header to be able to use search.dmoz.org


===========================================================================
== 2.0.2   Website
===========================================================================

The official website of the Personal Open Directory is:
 http://grohol.com/downloads/
where you can not only download the latest version of this script,
but also the MaxComm script which allows you to include links to
Amazon websites and maximise any commission you may earn.

The website also has a forum (also known as a bulletin board or
chat room) where problems can be posted and responded to. The authors
of POD can a regular eye on the forum and may be on hand to answer
any questions you may have - but please remember that they do have
do jobs and may not be able to answer your question immediately. Often
you may find that somebody else answers your question before them!
Registration on the forum is free and is recommended if you post
your questions there - you can personalise it so that responses
also get sent to your email address.
