==============================================================================
== 3.2     Configuration
==============================================================================

First of all, you need to configure POD so that it knows about your
server setup and that it looks how you want it to look.

If you are upgrading from a prior version of POD - please see section
'4.0 - Upgrading' for details

This can be done by editing the file 'podvars.pm' in any conventional
text editing package and editing various variables. These are listed in
the following sections.


==============================================================================
== 3.2.1   Script Configuration
==============================================================================

These variables change how the script operates, points the script towards
your files etc.

 $pod_progname
  This is the filename of your script.
  Example: $pod_progname="pod.cgi";
            $pod_progname="pod.pl";
  
 $pod_urlpath
  Where is your script held on your server. This variable should reflect the
  URL path instead of the server path.
  Example: $pod_urlpath="/cgi-bin/pod/";
  NOT    : $pod_urlpath="/home/user/public_html/cgi-bin/pod/";
  
 $pod_fullpath
  Usually you don't need to change this setting, but you may find it useful
  if you start tinkering with your script.
  Default: $pod_fullpath="$pod_urlpath$pod_progname";
  
 $pod_logging
  If you would like to record what a user searches on, set $pod_logging to
   a full path and filename of a logfile to write the the data into.
  The data is written in the following format:
   01 Mar 2000|risc os software
  When the user searched for 'risc os software' on the 1st of March.
  This file has got to be writable by the server.
  If you do not want to log searches, then just ensure that the
  line configuring $pod_logging is preceded with a # symbol.
  Example: $pod_logging="/home/httpd/logs/pod_searches.log"; # for logging
  Default: #$pod_logging="/home....              #  to disable logging
  
 $pod_templatedir
  When POD creates a page for a visitor, it uses two files - header.html
  and footer.html to add a header and footer to the page - these files
  could contain anything from a corporate logo to a full explanation of
  the meaning of life. But to use these iles, POD needs to know where they
  are, so set this variable to the server path of the location of the
  header.html and footer.html files
  Example: $pod_templatedir="/home/user/public_html/cgi-bin/pod/templates/";
  NOT    : $pod_templatedir= /cgi-bin/pod/templates";
  
 $pod_usequery
  This single variable changes the entire way POD works. In its default
  configuration, POD is used in the following manner:
   /cgi-bin/pod.cgi?dir=/Arts/Entertainment
  which will return the Arts/Entertainment category of the ODP. But, for
  some reason, you may not want the category to be passed to the script in
  what is called the 'Query_String', then just set $pod_usequery=1; and
  then POD will expect to be called thusly:
   /cgi-bin/pod.cgi/Arts/Entertainment
  using the Path_info variable. This can come in handy if you want to make
  it seem like the script is a directory on your server.
  Example: $pod_usequery=0;                     # for default operation
           $pod_usequery=1;                     # for 'alternative' operation
  Default: $pod_usequery=0;
  
 @prefixes
  Not used in version v1.70d
  
 $pod_proxy
  If you have a http proxy you wish the script to use (as may be the case
  if you are behind a firewall or you are running a http-proxy yourself and
  want to make use of it to reduce the load on the Dmoz.Org server), you
  just configure the URL of the proxy here, complete with port number
  if appropriate. If you keep this variable undefined by prefixing it
  with a # symbol, then POD will not attempt to use a proxy
  Example: $pod_proxy="http://proxy.sn.no:8001/"; # to enable
  Default: #$pod_proxy="http...";                 # disabled
  
 $pod_expire_in
  To be 'cache and proxy friendly', as of version 1.70d, POD now returns
  the Expires: header. What this does is allows the file returned to be
  held on a third-party server (called a 'cache' or 'proxy') for a set
  time. If somebody requests the same file within the time, then the
  file is returned from the cache instead of being fetched anew from your
  server.
  The setting is given in the format +[length of time][s|m|h|d|y] - this
  means that you can set +30s (s=seconds) so the page will be held for
  30 seconds, +10m (m=minutes) for the page to be held for 10 minutes,
  +1h (h=hour) for it to be held for 1 hour, +2d for two days and +1y for
  it to be held for one year! You can also configure 'now' which means
  the the file won't be held on the cache at all.
  Example: $pod_expire_in="now";    # don't allow caching
           $pod_expire_in="+1h";    # expire in 1 hour
  Default: $pod_expire_in="+10m";   # expire in 10 minutes
  
 $pod_useragent
  This string is sent to the the Dmoz.org server to identify itself -
  this may come in handy if the administrators of the server decide
  to block certain User-Agents (unlikely, but we're now able to
  pre-emptive them). This contents of this string could also be used
  to help them collect statiscal information. You shouldn't really
  need to change this setting from the default unless you are getting
  really strange messages from the server.
  Example: $pod_useragent="Mozilla/5.01 (compatible; Netscape; Windows 2000)";
               # identify ourselves as Netscape for Windows 2000
           $pod_useragent="Mozilla/3.01 (compatible; POD/ODP $pod_version)";
               # identify ourselves as being Mozilla 3.01 compatible
  Default: $pod_useragent="POD_ODP/$pod_version"; 
 
 $pod_debug
  Setting this string to '1' causes a load of debugging information to
  be shown when the script is run. This can help you and others diagnose
  any problems you may be having. In normal usage, this should be set
  to '0'.
  Example: $pod_debug="1"; # cause debug information to be shown
  Default: $pod_debug="0"; # no debug information
 
 $pod_modperl
  If you know you have mod_perl installed on your Apache server, then
  set $pod_modperl to 1 and the script will then use mod_perl to speed
  up execution of the script. If you either don't have mod_perl or
  have no idea what it is, then leave this variable alone.
  (not tested in version 1.70d)
  Example: $pod_modperl="1";  # use mod_perl
  Default: #$pod_modperl="1"; # don't use mod_perl

[new in v1.72d]
 $pod_headersuppress
  In normal operation, POD returns a full set of HTTP headers to the
  users browser, but if you are using SSI (server side includes) to
  include the output of POD, you don't really want this. So just set
  pod_headersuppress to '1' and it will disable the full http headers.
  POD will then only return the Content_Type header.
  Example: $pod_headersuppress='1'; # disable http headers
  Default: #$pod_headersuppress='1'; # don't disable headers
  
==============================================================================
== 3.2.2   Style Configuration
==============================================================================

These variables change the look of the html that is sent to the visitor
to your website.

 $pod_myhome
  Where is the homepage of your website. This is where the 'home' button
  will take the user to when they click on it.
  Example: $pod_myhome="http://www.beebware.com/";

 $pod_name
  What is the name of your personal open directory? This appears at the
  top of the page.
  Example: $pod_name="The Beebware web directory";
  Default: $pod_name="Personal Open Directory (POD)";
 
 $pod_shortname
  A shorten version of the name of your personal open directory. This
  is used in places where the name of the directory should appear but
  is short of space. For this reason, try and keep the shortened name
  to less than 10 characters (ideally the name should be between three
  and seven characters).
  Example: $pod_shortname="Directory";
  NOT    : $pod_shortname="My Directory";
  Default: $pod_shortname="MyPOD";
  
 $pod_fontface
 $pod_fontsize
  Not used in version 1.70d
  
 $pod_titlestyle
  This defines how the title string ($pod_name) looks by changing
  using the font command. Please remember that this string will be
  prefixed by '<font ' so only use valid tags.
  Example:$pod_titlestyle='size="-1" color="red"';
  Default:$pod_titlestyle='face="arial,helvetica" size="+1" color="#000088"';
  
 $pod_frontheader
  How do you want the front page to look? This string replaces the
  odphead.gif and opendir.gif images provided by the DMoz server.
  You can replace it with a graphic of your own or a string of text.
  Example:$pod_frontheader='<img src="/img/header.gif">';
  Default:$pod_frontheader="<font $pod_titlestyle><b>$pod_name</b></font>";
  
 $pod_coolicon
  If you don't like the little stars that the ODP uses to signify a
  'cool site' (ie a site that is 'above the average standard'), then you
  can replace them by inserting your own string here. If you want to keep
  the original stars, then prefix this line with a # symbol.
  Images should not really be any wider than 15 pixels nor higher than 16.
  Example:$pod_coolicon="<strong>&lt;&lt;Cool Site&gt;&gt;</strong>";
           # A cool site will be shown by <<Cool Site>>
          $pod_coolicon='<img src="/imgs/mycoolicon.gif" width="15"
          height="16" alt="[Cool]"> &nbsp;';
           # The replace it with your the image mycoolicon.gif
  Default:#$pod_coolicon="<strong>&lt;&lt;Cool Site&gt;&gt;</strong>";
           # Because it is prefixed by a hash symbol you will keep the
             original stars
 
 $pod_killmozzie
  Don't like those little lizards which appear in the bottom right hand
  corner of the screen. Well now you can take a chainsaw to them and
  cut them out. Or, you could even replace them with your own image
  or text. To keep the lizards (called 'mozzies') then prefix this
  line with a # symbol.
  Example:$pod_killmozzie='This is a Mozzie free site';
           # To have the text 'This is a Mozzie free site' appear instead
          $pod_killmozzie='<img src="/mylizard.gif">';
           # To have the image 'mylizard.gif' appear
  Default:#$pod_killmozzie='<font size="-1">This is a Mozzie free
          site</font>';
           # Keep the lizards because it the line is prefixed with a
             hash symbol
 
 $pod_international
  If you are having trouble accessing the World/ categories on your
  site, you just have to set $pod_international to 1 and the script
  will try and improvise. If you have no such problems, then leave
  this line commented out to ensure the script runs a bit faster
  Example:$pod_international=1; # use this if you are having trouble
                                  with the world categories
  Default:#$pod_international=1; # because it is prefixed by a hash
           symbol, the variable setting is ignored and the script
           doesn't try to improvise.
 
 $pod_boxes_editorlogin
 $pod_boxes_linklogin
 $pod_boxes_attribution
  Not used in version v1.70d
  
 $pod_boxes_editors
  Normally at the bottom of the category pages, there is a list of
  editors which created that category. If you want to remove the
  list of editors (and we urge you not to), then set pod_boxes_editors
  to '1' to remove their only credit.
  Example: $pod_boxes_editors="1";  # remove credit to those volunteers
  Default: #$pod_boxes_editors="1"; # keep their attribution

==============================================================================
== 3.2.3   Table Configuration
==============================================================================

To change the look of various tables produced, change these settings
to reflect the appropriate color/colour. These variables all take either
a color name (such as 'red','white' etc) or a hexadecimal RGB color
value (ie, you could either use 'red' or '#ff0000', 'green' or '#00ff00',
'white' or '#ffffff')

 $pod_lighttable
  At the top of the page, there is a table containing two bars - the
  top bar includes the title of your directory (as defined by the variable
  $pod_name - see section 3.2.2 ). This variable sets the background
  color of this bar.
  Default:$pod_lighttable="#FFFFDD";
 
 $pod_lighttableb
  At the top of the page, there is a table containing two bars - the
  bottom bar contains the 'home','Link','About' anchors. This variable
  sets the background color of this lower bar.
  Default:$pod_lighttableb="#EEEEFF";
 
 $pod_darktable
  Not used in version v1.70d
  
 $pod_medtable
  This tag sets the color of the background of the Amazon/Maxcomm
  bar for the booklinks. This tag only gets used if you have enabled
  the usage of the Maxcomm auto-book linking feature (see section
  3.2.4 - Amazon configuration)
  Default:$pod_medtable="#FFFFCC";

 $pod_tabletext
  Not used in version v1.70d
  
 $pod_editortable
  This sets the background color of the bar at the bottom of the page
  which includes the names of the various category editors. It also
  sets the color of the box where the links to the Description and FAQ
  appear if these are appropriate to the category.
  Default:$pod_editortable="#CCCCFF";
  
 $pod_faqtable
  This variable sets the background color of the table that the various
  category FAQs appear on.
  Default:$pod_faqtable="#f0f0f0";
  
 $pod_faqtext
  This variable (unlike the others in this section) sets the text color.
  This text color relates to the text on the FAQ pages.
  Default:$pod_faqtext="red";
  
 $pod_addtable
  Do you want to change the background color on the 'Add A Page' pages?
  Yes! Then change this setting here:
  Default:$pod_addtable="#ffff00";


==============================================================================
== 3.2.4   Amazon Configuration
==============================================================================

If you have the MaxComm script (see section '2.0.2 - Website') then this
is where you can configure the POD script so that it interacts with
the MaxComm script to include appropriate links to Amazon and help
maximise any commission you may earn.

 $pod_whichamazon
  This variable defines which Amazon server you want the script to use.
  If you do not want to use the MaxComm script, or you can't, then just
  prefix this line with a # symbol to disable the auto-linking
  feature.
  Please ensure you have the appropriate MaxComm script installed
  on your server - it is no good setting $pod_whichamazon to
  http://www.amazon.com if you've got the script to http://www.amazon.de
  installed!
  Example:$pod_whichamazon="http://www.amazon.co.uk"; # UK server
          $pod_whichamazon="http://www.amazon.com";   # US server
  Default:#$pod_whichamazon="http://www.amazon.com";  # Disabled
  
 $pod_amazon_ID
  Okay, you've got the MaxComm script installed and working on its
  own have you? But what is your affiliate/associates ID? If you
  don't configure this correctly, somebody else may get your commissions!
  Example:$pod_amazon_ID="beebwareinternat";
  Default:$pod_amazon_ID="";
  
 $pod_maxcomm
  But where is your MaxComm script? What URL has to be accessed to
  run it? Set the appropriate URL here:
  Example:$pod_maxcomm="/cgi-bin/pod/maxcomm.cgi";
  NOT    :$pod_maxcomm="/home/user/public_html/cgi-bin/pod/maxcomm.cgi";
            # This is a server path, not a URL
  Default:$pod_maxcomm="/cgi-bin/pod/maxcomm.cgi";
  
 $pod_bookstorename
  What do you want to show up as the name of the bookstore? This could
  be something like 'Amazon.Com' or 'Amazon in the States'.
  Example:$pod_bookstorename="Amazon UK";
  Default:$pod_bookstorename="Amazon.Com";
  
 $pod_amazon_missing
  If you find that for some strange reason your links to Amazon are
  missing the last character (this sometimes happens on certain
  system setups), then just uncomment this line and the script will
  try and work around the problem. If there isn't a problem, then
  just comment out the line by prefixing it with a # symbol.
  Example:#$pod_amazon_missing=1;    # no problems
          $pod_amazon_missing=1;     # got problems - enable workaround
  Default:#$pod_amazon_missing=1;    # no problems


==============================================================================
== 3.2.5   Header and Footers
==============================================================================

Changing the headers and footers is only recommended for people that
have had some prior experience of programming with Perl. It is not
something a 'first cgi-script' person should attempt to modify.

You may find it necessary to change the 'header.html' and 'footer.html'
text to 'header.htm' and 'footer.htm' if your server limits you in the
length of filename extensions.

You could also include the html markup directly in these functions -
but, again, this is not to be recommended for novices.

The main program expects these functions to return a text variable
that contains the header or footer. If you remove or modify the
attibution to the authors, please remember to put an appropriate
copyright message somewhere on your site (as per section '1.0.2 -
Copyright Notice'). Failure to include the attribution to the authors
is a breach of the terms of use of the Personal Open Directory program.
